package com.reactive.repostiory;// The following import statements will be required within DemoApplication.java

import com.reactive.model.Task;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

// Tasks Repository
public interface TasksRepository extends ReactiveCrudRepository<Task, Integer> {
}