package com.reactive.model;// The following import statements will be required within DemoApplication.java

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
 
// Task Model mapped to the tasks table
@Data
@RequiredArgsConstructor
@Table("tasks")
public class Task {
   @Id
   private Integer id;
   @NonNull
   private String description;
   private Boolean completed;
}