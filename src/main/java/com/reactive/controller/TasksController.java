package com.reactive.controller;// The following import statements will be required within DemoApplication.java

import com.reactive.model.Task;
import com.reactive.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

// Tasks controller class
@RestController
@RequestMapping("/tasks")
class TasksController {
 
   @Autowired
   private TaskService service;
 
   @GetMapping()
   public ResponseEntity<Flux> get() {
       return ResponseEntity.ok(this.service.getAllTasks());
   }
 
   @PostMapping()
   public ResponseEntity<Mono> post(@RequestBody Task task) {
       if (service.isValid(task)) {
           return ResponseEntity.ok(this.service.createTask(task));
       }
       return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
   }
 
   @PutMapping()
   public ResponseEntity<Mono> put(@RequestBody Task task) {
       if (service.isValid(task)) {
           return ResponseEntity.ok(this.service.updateTask(task));
       }
       return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
   }
 
   @DeleteMapping()
   public ResponseEntity<Mono> delete(@RequestParam int id) {
       if (id > 0) {
           return ResponseEntity.ok(this.service.deleteTask(id));
       }
       return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
   }
}