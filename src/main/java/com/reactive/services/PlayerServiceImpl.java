package com.reactive.services;

import com.reactive.model.Player;
import com.reactive.repostiory.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService
{
    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public Flux<Player> add(List<Player> players) {
        playerRepository.saveAll(players).subscribe();
        return null;
    }
}
