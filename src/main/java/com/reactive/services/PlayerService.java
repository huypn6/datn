package com.reactive.services;

import com.reactive.model.Player;
import reactor.core.publisher.Flux;

import java.util.List;

public interface PlayerService {
   public Flux<Player> add(List<Player> players);
}
